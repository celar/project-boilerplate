#ES6 Frontend Project Boilerplate


## Introductie

Dit is een schone boilerplate met daarin de meest gebruikte grunt tasks voor het starten van een Frontend project.
Deze project setup is speciaal voor ES6 ingericht. Hierbij gebruiken we Browserify om ES6 javascript om te zetten naar ES5.
Ook esLint is een betere optie ipv jsHint voor ES6 code.

Bij het runnen van je project wordt er een 'dist' folder aangemaakt. Dit is de plek waar alle gecompileerde code neergezet
zal worden. Pas geen code aan in deze folder. Doe dat in de 'src' folder.

## Installatie
Om het project op te starten doe je het volgende.
Update al je gems. Dit omvat o.a. het update van je SASS versie.

```bash
gem update --system
```

```bash
bower install
```

```bash
npm install
```

## Requirements

| Requirements                                                              | Versions |
| ------------------------------------------------------------------------- | -------- |
| [Sass](http://sass-lang.com/install)                                      | ~3.4.14  |
| [Ruby] (https://www.ruby-lang.org/en/downloads/)                          | ~2.2.2   |
| [Node.js](http://nodejs.org)                                              | ~4.0.0   |
| [Grunt.js](http://gruntjs.com)                                            | ~0.4.5   |

## Grunt plugins

Use the ``npm`` command to download the Grunt dependencies : ``npm install``.

| Plugins                                                                   | Versions |
| ------------------------------------------------------------------------- | -------- |
| [grunt]                                                                   | ~0.4.5   |
| [grunt-eslint]                                                            | latest   |
| [babel-eslint]                                                            | latest   |
| [grunt-contrib-sass]                                                      | latest   |
| [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch)     | latest   |
| [grunt-contrib-clean]																											| latest   |
| [grunt-contrib-copy]																											| 0.5.0    |
| [grunt-contrib-uglify]																										| latest   |
| [grunt-contrib-connect]                                                   | 0.6.0    |
| [grunt-fontface]												    															| 0.9.0    |
| [grunt-webfont]                                                           | latest   |
| [grunt-include-replace]											    													| latest   |
| [babelify]											    																			| ~6.1.3   |
| [grunt-browserify]													    													| ~4.0.0   |
| [time-grunt]																															| 1.0.0    |

## Bower packages

Use the ``bower`` command to download the Bower dependencies : ``bower install``.

| Libraries                                                                 | Versions |
| ------------------------------------------------------------------------- | -------- |
| [jquery](http://jquery.com)                                               | ~1.11.1  |
| [modernizr](http://modernizr.com)                                         | ~2.8.3   |
| [bourbon](http://bourbon.io/)                                             | =4.0.2   |
| [normalize-scss](https://github.com/JohnAlbin/normalize-scss)             | 3.0.2    |


## Runnen van Grunt
Het standaard draaien van grunt tasks (default) kan door middel van onderstaande commando.
```bash
grunt
```
Deze zal de taken die gedefineerd staan in /grunt-tasks/default.js uitvoeren.

## Bower
Er wordt bower gebruikt voor het beheren van bijvoorbeeld modernizr en jquery packages.
In 'bower.json' staan de packages die geinstalleerd worden. Om deze te gebruiken kopieren we ze via de grunt tasks
uglify.

## Bourbon
Dit is een mixin library voor sass. Deze bevat een aantal handige mixins.

## ICON FONTS
Je kan een extra icon toevoegen aan de src/assets/icons map. Dit moet een svg bestand zijn van 512x512 pixels. De naam is ook de classnaam die je in de css gebruikt.

## Grunt tasks
* **compile**: generate all css. Minified css and js. Copies everything to the release directory
* **default**: does compile and watch on all files and fires a node server
* **build**: does compile and production browserify and ultimaly ugilfy javascripts.
