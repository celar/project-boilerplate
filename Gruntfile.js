/*globals module*/

// bronnen:
// http://gruntjs.com/sample-gruntfile

module.exports = function (grunt) {

	'use strict';

	grunt.initConfig({
		pkg: {
			destinationFolder: 'dist',
			portnr: 8901
		},
		modernizrCustomBuild: {
			dest: '<%= pkg.destinationFolder %>/js/vendor/modernizr-build.js'
		},
		eslint: {
			target: ['Gruntfile.js', 'src/**/*.js']
		},
		sass: {
			dist: {
				options: {
					sourcemap: 'auto'
				},
				files: {
					'<%= pkg.destinationFolder %>/css/main.css': 'src/scss/main.scss'
				}
			}
		},
		clean: {
			delivery: ['<%= pkg.destinationFolder %>', 'src/generated']
		},
		fontface: {
			options: {
				fontDir: 'src/assets/fonts',
				template: '@font-face{' +
				'font-family: "{{font}}";' +
				'src: url("../fonts/{{font}}.eot");' +
				'src: url("../fonts/{{font}}.eot?#iefix") format("embedded-opentype"),' +
				'url("../fonts/{{font}}.woff") format("woff"),' +
				'url("../fonts/{{font}}.ttf")  format("truetype");' +
				'}',
				outputFile: 'src/generated/scss/_fonts.scss'

			}
		},
		webfont: {
			icons: {
				src: 'src/assets/icons/*.svg',
				dest: 'src/generated/fonts',
				destCss: 'src/generated/scss',
				options: {
					fontFilename: 'webfont-icons',
					hashes: false,
					stylesheet: 'scss',
					engine: 'node',
					order: 'woff, eot, svg, ttf'
				}
			}
		},
		includereplace: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src/html/',
					src: ['*.html'],
					dest: '<%= pkg.destinationFolder %>',
					ext: '.html'
				}]
			}
		},
		copy: {
			favicon: {
				expand: true,
				flatten: true,
				src: ['src/assets/favicon.ico'],
				dest: '<%= pkg.destinationFolder %>'
			},
			fonts: {
				expand: true,
				flatten: true,
				src: ['src/assets/fonts/*', 'src/generated/fonts/*'],
				dest: '<%= pkg.destinationFolder %>/css/fonts/'
			},
			images: {
				expand: true,
				flatten: true,
				src: ['src/assets/img/*'],
				dest: '<%= pkg.destinationFolder %>/img'
			},
			svg: {
				expand: true,
				flatten: true,
				src: ['src/assets/svg/*'],
				dest: '<%= pkg.destinationFolder %>/svg'
			},
			js: {
				files: {
					'<%= pkg.destinationFolder %>/js/vendor/jquery.min.js' : 'bower_components/jquery/dist/jquery.min.js'
				}
			}
		},
		uglify: {
			dist: {
				files: {
					'<%= pkg.destinationFolder %>/js/bundle.min.js': '<%= pkg.destinationFolder %>/js/bundle.js',
					'<%= pkg.destinationFolder %>/js/vendor/modernizr.min.js': '<%= pkg.destinationFolder %>/js/vendor/modernizr-build.js'
				}
			}
		},
		browserify: {
			dev: {
				options: {
					transform: ['babelify']
				},
				files: {
					'<%= pkg.destinationFolder %>/js/bundle.js' : ['./src/js/**/*.js']
				}
			},
			production: {
				options: {
					debug: false,
					transform: ['babelify']
				},
				src: ['/src/js/main.js'],
				dest: '<%= pkg.destinationFolder %>/js/bundle.js'
			}
		},
		connect: {
			server: {
				options: {
					base: '<%= pkg.destinationFolder %>',
					hostname: 'localhost',
					port: '<%= pkg.portnr %>',
					open: true
				}
			}
		},
		watch: {
			files: ['<%=eslint.target %>', 'src/**/*.html', 'src/**/*.scss'],
			tasks: ['sass:dist', 'includereplace', 'eslint', 'browserify:dev', 'uglify:dist']
		}
	});

//
// Met deze custom task gaan we een custom build maken van Modernizr.
//
	grunt.registerMultiTask('modernizrCustomBuild', 'Create a version of Modernizr from Grunt', function() {
		var done = this.async();
		var config = require('./modernizr.json');
		var modernizr = require('./bower_components/modernizr/lib/cli');
		var dest = this.data;

		modernizr.build(config, function(output) {
			grunt.file.write(dest, output);
			done();
		});
	});

	grunt.loadNpmTasks('grunt-eslint');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-fontface');
	grunt.loadNpmTasks('grunt-webfont');
	grunt.loadNpmTasks('grunt-include-replace');
	grunt.loadNpmTasks('grunt-browserify');

	grunt.registerTask('compile', [
		'clean',
		'modernizrCustomBuild',
		'fontface',
		'webfont:icons',
		'includereplace:dist',
		'copy:fonts',
		'copy:favicon',
		'copy:images',
		'copy:svg',
		'copy:js',
		'eslint',
		'sass:dist'
	]);
	grunt.registerTask('default', ['compile', 'connect:server', 'watch']);
	grunt.registerTask('build', ['compile', 'browserify:production', 'uglify:dist']);

};